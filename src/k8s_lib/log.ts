const { getDateUTC } = require('./utils');

require('dotenv').config();

module.exports.logMsg = (
  error: boolean,
  message: object,
  request_id?: string,
  request_session_id?: string,
) => {
  const msg: any = {
    level: error ? 50 : 30,
    'x-request-id': request_id,
    'request-session-id': request_session_id,
    app: process.env.APP_NAME,
    time: getDateUTC(),
  };

  if (error) {
    msg.error = message;
  } else {
    msg.msg = message;
  }

  if (error) {
    console.error(JSON.stringify(msg));
  } else {
    console.log(JSON.stringify(msg));
  }
};
