import { Injectable, BadRequestException } from '@nestjs/common';

@Injectable()
export class ApiService {
    constructor(){}

    async testOk(): Promise<string> {
        return `Test Ok`;
    }

    async testBad(): Promise<string> {
        throw new BadRequestException(`TEST BAD REQUEST.`);
    }
}
