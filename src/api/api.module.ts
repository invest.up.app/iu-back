import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MyLogger } from 'src/logger/MyLogger';
import { ApiController } from './api.controller';
import { ApiService } from './api.service';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ResponsInterceptor } from 'src/interceptor/respons.interceptor';
import { UserModule } from './user/user.module';

@Module({
    imports: [
      ConfigModule.forRoot(),
      UserModule,
    ],
    providers: [
        // MyLogger,
        ApiService,
        {
          provide: APP_INTERCEPTOR,
          useClass: ResponsInterceptor,
        },],
    controllers: [ApiController],
})

export class ApiModule {}
