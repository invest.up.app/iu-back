import { Controller, Get } from '@nestjs/common';
import { ApiService } from './api.service';

@Controller('api')
export class ApiController {

    constructor(
        private apiService: ApiService,
    ) {}

    @Get('ok')
    testOk(): Promise<string> {
        return this.apiService.testOk();
    }

    @Get('bad')
    testBad(): Promise<string> {
        return this.apiService.testBad();
    }
}
