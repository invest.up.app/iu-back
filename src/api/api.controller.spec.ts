import { Test, TestingModule } from '@nestjs/testing';
import { ApiController } from './api.controller';
import { ApiService } from './api.service';

describe('Api Controller', () => {
  let controller: ApiController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ApiController],
      providers: [ApiService],
    }).compile();

    controller = app.get<ApiController>(ApiController);
  });

  it('should be defined', async () => {
    const result = await controller.testOk();
    expect(result).toBe('Test Ok');
  });
});
