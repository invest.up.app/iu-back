import { Injectable, BadRequestException } from '@nestjs/common';
import { UserAuthPayload } from 'src/dto/user.dto';
import { Request } from 'express';

import { databaseQuery } from '../../helper';
var jwt = require('jsonwebtoken');

@Injectable()
export class UserService {

    constructor () {}

    async auth(
        req: Request,
        payload: UserAuthPayload,
    ): Promise<any> {

        const {
            login,
            password,
        } = payload;


        const address = req.ip;
        const client = req.headers['user-agent'];

        const result = await databaseQuery('pkg_user', 'user_auth', [login, password, address, client]);
        
        let session_id;

        try {
            session_id = result[1][0][0];
        } catch (err) {
            throw new BadRequestException('Bad Request');
        }
        
        const token = jwt.sign({ session_id }, process.env.SALT);

        return {token};
    }

}
