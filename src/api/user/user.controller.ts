import { Controller, Post, Req, Body } from '@nestjs/common';
import { UserService } from './user.service';
import { Request } from 'express';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { UserAuthPayload } from '../../dto/user.dto';

@Controller('user')
export class UserController {

    constructor(
        private service: UserService,
    ){}

    @Post('auth')
    @ApiTags('Auth')
    @ApiOperation({
      summary: 'User authorization',
      operationId: 'userAuth',
    })

    auth (
        @Req() req: Request,
        @Body() payload: UserAuthPayload,    
    ): Promise<any> {
        return this.service.auth(req, payload);
    }
}
