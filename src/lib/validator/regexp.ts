export const dateRegex = new RegExp(
  '^\\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$',
);
export const passRegex = new RegExp(
  '^[a-zA-Z0-9[\\]\\/\\-~!@#$%^&*()+_.,\'"{}|?<>]{6,32}$',
);
