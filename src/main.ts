import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AllExceptionsFilter } from './filter/allExceptionsFilter.filter';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
// import { MyLogger } from './logger/MyLogger';



async function bootstrap() {

  const port = Number(process.env.APP_PORT) || 4000;

  const app = await NestFactory.create(AppModule, {
    // logger: new MyLogger()
  });

  // app.useLogger(app.get(MyLogger));
  // logger.log({port})

  if (process.env.NODE_ENV !== 'production') {
    const options = new DocumentBuilder()
      .setTitle('BSP API')
      .setDescription('BSP API reference')
      .setVersion('1.0')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('docs', app, document);
  }


  app.useGlobalFilters(new AllExceptionsFilter());

  app.enableCors();

  // app.useGlobalPipes(new ValidationPipe());
  
  await app.listen(port);
}
bootstrap();
