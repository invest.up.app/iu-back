import { LoggerService } from '@nestjs/common';
// import { logMsg } from './../k8s_lib/log';
// const { logMsg } = require('./../k8s_lib/log');

export class MyLogger implements LoggerService {
  log(trace: string, message: string) {
      console.log({trace, message})
  }

  warn(trace: string, message: string) {
    console.log({trace, message})
  }

  error(trace: string, message: string) {
    console.log({trace, message})
  }

  debug(trace: string, message: string) {
    console.log({trace, message})
  }

  verbose(trace: string, message: string) {
    console.log({trace, message})
  }
}
