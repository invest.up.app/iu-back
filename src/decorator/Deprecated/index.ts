import {MethodNotAllowedException, SetMetadata} from "@nestjs/common";

const CustomDeprecated  = (): any =>
    SetMetadata(
      'deprecated',
      () => {
          throw new MethodNotAllowedException('Method deprecated');
      }
    );

export {CustomDeprecated};
