# Модуль для работы с переменными окружения

```typescript

import { ConfigService } from '../config/config.service';
import Env from './api.env.class'; // class-validator с описанием требуемых полей

...

  constructor (
    private readonly configService: ConfigService,
  ) {
    this.env = this.configService.getObject(Env);
  }
...

```