import { validateSync } from 'class-validator';
const log = require('../k8s_lib/log');

export class ConfigService {
  get(key: string): string {
    return process.env[key];
  }

  getObject<T>(Env: { new (...args: any[]): T }): T {
    const ret = new Env();

    Object.keys(process.env).forEach(key => {
      ret[key] = process.env[key];
    });

    const errors = validateSync(ret);

    if (errors.length) {
      errors.forEach(err => {
        log.logMsg(
          true,
          `env variable '${err.property}' is incorrect, ${Object.values(
            err.constraints,
          )}`,
        );
      });

      process.exit(1);
    }

    return ret;
  }
}
