

export interface IResponseOk<T> {
    status: string,
    result: T,
}