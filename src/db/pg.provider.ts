// import { createConnection } from 'typeorm';
const pgp = require('pg-promise')();


const type = 'postgres';
const host = process.env.APP_DB_HOST;
const port = Number(process.env.APP_DB_PORT);
const username = process.env.APP_DB_USERNAME;
const password = process.env.APP_DB_PASSWORD;
const database = process.env.APP_DB_NAME;

const db = pgp(`postgres://${username}:${password}@${host}:${port}/${database}`);


export default db;
