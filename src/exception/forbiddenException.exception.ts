import { HttpException, HttpStatus } from '@nestjs/common';

export class ForbiddenException extends HttpException {
  public message: string;

  constructor() {
    super('Access denied', HttpStatus.FORBIDDEN);
    this.message = 'Forbidden';
  }
}
