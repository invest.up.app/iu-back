import { HttpException, HttpStatus } from '@nestjs/common';

export class DatabaseException extends HttpException {
  public message: 'object';

  constructor(message?: string | object | any) {
    super('Database error', HttpStatus.BAD_REQUEST);
    this.message = message;
  }
}
