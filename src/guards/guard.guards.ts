import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Inject,
  ForbiddenException,
} from '@nestjs/common';
// import { Repository } from 'typeorm';
import { Reflector } from '@nestjs/core';
import { databaseQuery } from '../helper';
// import { UserGuardDTO } from '../dto/auth.dto';

const jwt = require('jsonwebtoken');

@Injectable()
export class UserGuard implements CanActivate {
  constructor(
    // @Inject('USER_REPOSITORY')
    // private userCheckRepository: Repository<UserGuardDTO>,
    // private userCheckRepository: Repository<Any>,
    // private readonly reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<any> {
  //   const deprecated = this.reflector.get<any>(
  //     'deprecated',
  //     context.getHandler(),
  //   );

  //   if (!!deprecated ) {
  //     deprecated();
  //   }
  //   const req = context.switchToHttp().getRequest();
  //   const permission = this.reflector.get<string[]>(
  //     'permission',
  //     context.getHandler(),
  //   );

  //   if (!req.headers.authorization) {
  //     throw new ForbiddenException({
  //       msg: 'Authorization header not found',
  //     });
  //   }

  //   const jwtToken = req.headers.authorization.split(' ');
  //   if (jwtToken[0].toLowerCase() !== 'bearer') {
  //     throw new ForbiddenException('Wrong authorization header');
  //   }
  //   const decoded = jwt.verify(jwtToken[1], process.env.APP_USER_JWT_SIGNATURE);
  //   const sessionId = decoded.session_id;

  //   let sessionCheck;
  //   try{
  //     sessionCheck = await databaseQuery(
  //       this.userCheckRepository,
  //       'pkg_auth',
  //       'session_check',
  //       [sessionId],
  //     );
  //   } catch (err) {
  //     throw new ForbiddenException('Session not found');
  //   }

  //   const userId = sessionCheck[1][0][0];
  //   // const permissionCheck = await databaseQuery(
  //   //   this.userCheckRepository,
  //   //   'pkg_user',
  //   //   'user_permission_check',
  //   //   [userId, permission, false],
  //   // );
  //   //
  //   // if (permissionCheck[1][0][0] === false) {
  //   //   throw new ForbiddenException({
  //   //     msg: 'Permission check failed',
  //   //     userId,
  //   //     permission,
  //   //   });
  //   // }

  //   const [userInfoBySession] = await databaseQuery(
  //     this.userCheckRepository,
  //     'pkg_user',
  //     'user_list',
  //     [userId, [userId]],
  //     true,
  //   );

  //   Object.assign(req.body, {
  //     permission,
  //     session_id: sessionId,
  //     userInfoBySession,
  //   });

    // return permissionCheck[1][0][0];
    return true;
  }
}
