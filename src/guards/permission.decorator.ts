import { SetMetadata } from '@nestjs/common';

const Permission = (permission?: string | string[]) =>
  SetMetadata(
    'permission',
    typeof permission === 'object' ? permission : [permission],
  );

export { Permission };
