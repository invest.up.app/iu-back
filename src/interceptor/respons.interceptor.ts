import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const { logMsg } = require('./../k8s_lib/log');

@Injectable()
export class ResponsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const res = context.switchToHttp().getResponse();
    const req = context.switchToHttp().getRequest();

    return next.handle().pipe(
      map(data => {
        res.statusCode = 200;
        const headers = { ...req.headers };

        headers['Content-Type'] = 'application/json';

        if (req.headers['x-request-id']) {
          headers['X-Request-Id'] = req.headers['x-request-id'];
        }

        // eslint-disable-next-line no-restricted-syntax
        for (const header of Object.keys(headers)) {
          res.setHeader(header, headers[header]);
        }

        const body = {
          status: 'ok',
          result: data,
        };

        logMsg(
          false,
          body,
          req.headers['x-request-id'],
          req.headers['request-session-id'],
        );

        return body;
      }),
    );
  }
}
