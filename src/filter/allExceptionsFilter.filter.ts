import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';

const { logMsg } = require('../k8s_lib/log');

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const res = context.getResponse();
    const req = context.getRequest();

    const errorCode =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const body = {
      status: 'error',
      errorCode,
      typeError: HttpStatus[errorCode].toLocaleLowerCase(),
      timestamp: new Date().toISOString(),
      path: req.url,
      'x-request-id': req.headers['x-request-id'],
      error: exception.message || 'unknown_error',
    };

    logMsg(
      true,
      body,
      req.headers['x-request-id'],
      req.headers['request-session-id'],
    );

    if (process.env.NODE_ENV === 'production') {
      body.error = body.error.message || 'unknown_error';
      body.path = undefined;
    }

    res.status(body.errorCode).json(body);
  }
}
