import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ApiModule } from './api/api.module';
import { ConfigModule } from './config/config.module';

@Module({
  imports: [ConfigModule, ApiModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
