import { IsEmail, IsString } from "class-validator";
import { ApiProperty } from '@nestjs/swagger';


export class UserAuthPayload {
    @ApiProperty({
        example: 'admin',
    })
    @IsString()
    login: string;

    @ApiProperty({
        example: '',
    })
    @IsString()
    password: string;
}
