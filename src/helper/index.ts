import db from "../db/pg.provider";

const databaseQuery = async (pkg: string, procedure: string, params: any, assoc: boolean = true) => {

    try {
        if (!Array.isArray(params)) {
            throw new Error('Params not array')
        }
        let queryParams = '';
        if (params.length > 0) {
            params.forEach( (elem: any, index: number) => {
                queryParams += index === 0 ? '' : ', '
                if (Array.isArray(elem)) {
                    queryParams += `Array[${elem}]`
                } else if (typeof(elem) === 'string') {
                    queryParams += `'${elem}'`
                } else if (elem === undefined) {
                    queryParams += `null`
                } else {
                    queryParams += `${elem}`
                }
            })
        }

        const queryResult = await db.query(`SELECT * FROM ${pkg}.${procedure}(${queryParams})`);

        const result = assoc ? pgAssoc(queryResult) : queryResult;

        return result;
    } catch (err) {
        console.error(err);
        return err;
    }
}

const pgAssoc = data => {
    if(data.length === 0 ) {
        return []
    }
    const names = Object.keys(data[0]);
    const values = data.map(row => Object.values(row));
    return [names, values];
  };

const getQueryFields = (queryResult: any[], fields: string[]) => {
    return fields.map((field: string) => {
        return queryResult[0][`${field}`]
    })
}

export {
    databaseQuery,
    getQueryFields,
}