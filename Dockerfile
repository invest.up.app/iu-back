FROM mhart/alpine-node:12.18.0

WORKDIR /app
COPY package.json .

# If you have native dependencies, you'll need extra tools
# RUN apk add --no-cache make gcc g++ python

RUN apk update --no-cache && apk add --no-cache imagemagick
RUN npm install --prod

COPY dist .

EXPOSE 5432
CMD ["node", "main.js"]


# FROM node:12

# WORKDIR /usr/src/app

# COPY package*.json ./

# RUN npm install

# COPY . .

# CMD ["node", "dist/main.js"]